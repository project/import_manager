<?php

function import_manager_drush_help($section) {
  switch ($section) {
    case 'drush:import-manager-list':
      return dt('List available imports.');

    case 'drush:import-manager-run':
      return dt('Run an import.');
  }
}

function import_manager_drush_command() {
  return array(
    'import-manager-list' => array(
      'callback' => 'import_manager_drush_list',
      'description' => 'List available imports.',
      'arguments' => array(
        '[module]' => 'Optional module name.',
      ),
    ),
    'import-manager-run' => array(
      'callback' => 'import_manager_drush_run',
      'description' => 'Run an import.',
      'arguments' => array(
        'function' => 'Function to run.',
        '[...]' => 'Additional arguments.',
      ),
    ),
  );
}

function import_manager_drush_list($module = NULL) {
  if (is_null($module)) {
    $modules = module_implements('imports');
  }
  else {
    $modules = array($module);
  }
  foreach ($modules as $module) {
    $info = drupal_parse_info_file(drupal_get_path('module', $module) .'/'. $module .'.info');
    foreach (module_invoke($module, 'imports') as $name => $group) {
      drush_print('');
      drush_print($info['name'] . ' - ' . $name);
      $functions = array();
      foreach ($group as $function => $options) {
        $functions[] = array(
          $function,
          $options['title'],
        );
      }
      drush_print_table($functions);
    }
  }
}

function import_manager_drush_run() {
  $args = func_get_args();
  $function = array_shift($args);
  import_manager_run($function, $args);
}
